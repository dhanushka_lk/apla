﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLA.Models;
using APLA.DAL;

namespace APLA.Controllers
{
    public class ReligionController : Controller
    {
        private AplaEntities db = new AplaEntities();

        //
        // GET: /Religion/

        public ActionResult Index()
        {
            return View(db.Religions.ToList());
        }

        //
        // GET: /Religion/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    Religion religion = db.Religions.Find(id);
        //    if (religion == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(religion);
        //}

        //
        // GET: /Religion/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Religion/Create

        [HttpPost]
        public ActionResult Create(Religion religion)
        {
            if (ModelState.IsValid)
            {
                db.Religions.Add(religion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(religion);
        }

        //
        // GET: /Religion/Edit/5

        //public ActionResult Edit(int id = 0)
        //{
        //    Religion religion = db.Religions.Find(id);
        //    if (religion == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(religion);
        //}

        ////
        //// POST: /Religion/Edit/5

        //[HttpPost]
        //public ActionResult Edit(Religion religion)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(religion).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(religion);
        //}

        //
        // GET: /Religion/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Religion religion = db.Religions.Find(id);
            if (religion == null)
            {
                return HttpNotFound();
            }
            return View(religion);
        }

        //
        // POST: /Religion/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Religion religion = db.Religions.Find(id);
            db.Religions.Remove(religion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}