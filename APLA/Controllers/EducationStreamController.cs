﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLA.Models;
using APLA.DAL;

namespace APLA.Controllers
{
    public class EducationStreamController : Controller
    {
        private AplaEntities db = new AplaEntities();

        //
        // GET: /EducationStream/

        public ActionResult Index()
        {
            return View(db.EducationStreams.ToList());
        }

        //
        // GET: /EducationStream/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    EducationStream educationstream = db.EducationStreams.Find(id);
        //    if (educationstream == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(educationstream);
        //}

        //
        // GET: /EducationStream/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /EducationStream/Create

        [HttpPost]
        public ActionResult Create(EducationStream educationstream)
        {
            if (ModelState.IsValid)
            {
                db.EducationStreams.Add(educationstream);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(educationstream);
        }

        //
        // GET: /EducationStream/Edit/5

        public ActionResult Edit(int id = 0)
        {
            EducationStream educationstream = db.EducationStreams.Find(id);
            if (educationstream == null)
            {
                return HttpNotFound();
            }
            return View(educationstream);
        }

        //
        // POST: /EducationStream/Edit/5

        [HttpPost]
        public ActionResult Edit(EducationStream educationstream)
        {
            if (ModelState.IsValid)
            {
                db.Entry(educationstream).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(educationstream);
        }

        //
        // GET: /EducationStream/Delete/5

        public ActionResult Delete(int id = 0)
        {
            EducationStream educationstream = db.EducationStreams.Find(id);
            if (educationstream == null)
            {
                return HttpNotFound();
            }
            return View(educationstream);
        }

        //
        // POST: /EducationStream/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            EducationStream educationstream = db.EducationStreams.Find(id);
            db.EducationStreams.Remove(educationstream);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}