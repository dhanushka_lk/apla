﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLA.Models;
using APLA.DAL;
using System.Web.Configuration;
using PagedList;

namespace APLA.Controllers
{
    public class CityController : Controller
    {
        private AplaEntities db = new AplaEntities();

        //
        // GET: /City/

        public ActionResult Index(string currentFilter, string searchString, int? page)
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var cities = from t in db.City
                           select t;

            if (!String.IsNullOrEmpty(searchString))
            {
                cities = cities.Where(t => t.CityName.ToLower().Contains(searchString.ToLower()));
            }

            var result = WebConfigurationManager.AppSettings["CityListPageSize"];
            int pageSize = 5;
            if (!string.IsNullOrEmpty(result))
            {
                pageSize = Convert.ToInt32(result);
            }

            int pageNumber = (page ?? 1);

            if (cities.ToList().Count > 0)
            {
                return View(cities.ToList().ToPagedList(pageNumber, pageSize));
            }
            else
            {
                return RedirectToAction("Index", "City", null);
            }
        }

        //
        // GET: /City/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    City city = db.City.Find(id);
        //    if (city == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(city);
        //}

        //
        // GET: /City/Create

        public ActionResult Create()
        {
            ViewBag.DistrictId = new SelectList(db.Districts, "Id", "DistrictName");
            return View();
        }

        //
        // POST: /City/Create

        [HttpPost]
        public ActionResult Create(City city)
        {
            if (ModelState.IsValid)
            {
                db.City.Add(city);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DistrictId = new SelectList(db.Districts, "Id", "DistrictName", city.DistrictId);
            return View(city);
        }

        //
        // GET: /City/Edit/5

        public ActionResult Edit(int id = 0)
        {
            City city = db.City.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            ViewBag.DistrictId = new SelectList(db.Districts, "Id", "DistrictName", city.DistrictId);
            return View(city);
        }

        //
        // POST: /City/Edit/5

        [HttpPost]
        public ActionResult Edit(City city)
        {
            if (ModelState.IsValid)
            {
                db.Entry(city).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DistrictId = new SelectList(db.Districts, "Id", "DistrictName", city.DistrictId);
            return View(city);
        }

        //
        // GET: /City/Delete/5

        public ActionResult Delete(int id = 0)
        {
            City city = db.City.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
        }

        //
        // POST: /City/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            City city = db.City.Find(id);
            db.City.Remove(city);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}