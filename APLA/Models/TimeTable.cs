﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace APLA.Models
{
    [Table("Apla_TimeTable")]
    public class TimeTable
    {
        public int Id { get; set; }
        [Range(2014, 2020)]
        public int year { get; set; }
        public Nullable<int> EducationStreamId { get; set; }
        [Required]
        public int SubjectId { get; set; }
        [Required]
        public int ClassTypeId { get; set; }
        [Required]
        [DataType(DataType.Time)]
        public DateTime time { get; set; }
        [Required]
        public String Day { get; set; }
        [Required]
        public string Venue { get; set; }
        public int TeacherId { get; set; }

        public virtual EducationStream EducationStream { get; set; }
        public virtual Subject Subject { get; set; }
        public virtual ClassType ClassType { get; set; }
        public virtual Teacher Teacher { get; set; }


       
    }
}