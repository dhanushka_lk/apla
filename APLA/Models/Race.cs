﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace APLA.Models
{
    [Table("Apla_Race")]
    public class Race
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}