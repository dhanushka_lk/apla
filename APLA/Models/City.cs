﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace APLA.Models
{
    [Table("Apla_City")]
    public class City 
    {
        public City()
        {

        }

        public int Id { get; set; }
        public int DistrictId { get; set; }
        public string CityName { get; set; }

        public virtual District District { get; set; }
    }
}