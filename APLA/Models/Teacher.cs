﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace APLA.Models
{
    [Table("Apla_Teacher")]
    public class Teacher
    {
        public Teacher()
        {
            this.Children = new HashSet<Children>();
            this.Subjects = new HashSet<Subject>();
            this.Institutes = new HashSet<Institute>();
            this.Occupations = new HashSet<Occupation>();
            this.EducationStreams = new HashSet<EducationStream>();
        }

        public int Id { get; set; }
        public string RegNo { get; set; }
        public string Name { get; set; }
        [Required]
        public string CommonName { get; set; }
        public string FullName { get; set; }
        public int CurrentDistrictId { get; set; }
        public int CurrentCityId { get; set; }
        public string CurrentLane { get; set; }
        public int PermanentDistrictId { get; set; }
        public int PermanentCityId { get; set; }
        public string PermanentLane { get; set; }
        public string Pho_R { get; set; }
        public string Pho_O { get; set; }
        [Required]
        [StringLength(10, MinimumLength = 10, ErrorMessage = "Invalid Phone No")]
        public string Pho_M { get; set; }
        public string Email { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Birth Day")]
        public Nullable<DateTime> Bday { get; set; }
        [RegularExpression(@"^\d{9}[V|v|x|X]$",ErrorMessage="Please enter a valid Id Number")]
        [StringLength(10)]
        public string NationalId { get; set; }
        public int RaceId { get; set; }
        public int ReligionId { get; set; }
        [Display(Name = "Is married")]
        public bool IsMarried { get; set; }
        public string PartnerName { get; set; }
        public Nullable<DateTime> PartnerBday { get; set; }
        public bool IsPartnerWorking { get; set; }
        public int UserId { get; set; }
        [UIHint("ActiveDeactive")]
        [Display(Name = "Status")]
        public bool Enabled { get; set; }
        public bool Approved { get; set; }

        [Display(Name = "FaceBook")]
        [Url]
        public string facebook { get; set; }
        [Display(Name = "Google Plus")]
        [Url]
        public string googlePlus { get; set; }
        [Display(Name = "Twitter")]
        [Url]
        public string twitter { get; set; }
        [Display(Name = "Linked In")]
        [Url]
        public string linkedIn { get; set; }
        [Display(Name = "You Tube")]
        [Url]
        public string youTube { get; set; }

        public virtual User User { get; set; }
        public virtual Race Race { get; set; }
        public virtual Religion Religion { get; set; }

        public virtual ICollection<Children> Children { get; set; }
        [Required]
        public virtual ICollection<Subject> Subjects { get; set; }
        public virtual ICollection<Institute> Institutes { get; set; }
        public virtual ICollection<Occupation> Occupations { get; set; }
        public virtual ICollection<EducationStream> EducationStreams { get; set; }
    }
}