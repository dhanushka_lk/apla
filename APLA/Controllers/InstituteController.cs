﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLA.Models;
using APLA.DAL;
using System.Web.Configuration;
using PagedList;

namespace APLA.Controllers
{
    public class InstituteController : Controller
    {
        private AplaEntities db = new AplaEntities();

        //
        // GET: /Institute/

        public ActionResult Index(string currentFilter, string searchString, int? page)
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var institutes = from t in db.Institutes
                         select t;

            if (!String.IsNullOrEmpty(searchString))
            {
                institutes = institutes.Where(t => t.Name.ToLower().Contains(searchString.ToLower()));
            }

            var result = WebConfigurationManager.AppSettings["InstituteListPageSize"];
            int pageSize = 5;
            if (!string.IsNullOrEmpty(result))
            {
                pageSize = Convert.ToInt32(result);
            }

            int pageNumber = (page ?? 1);

            return View(institutes.ToList().ToPagedList(pageNumber, pageSize));
        }

        //
        // GET: /Institute/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    Institute institute = db.Institutes.Find(id);
        //    if (institute == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(institute);
        //}

        //
        // GET: /Institute/Create

        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(db.City, "Id", "CityName");
            return View();
        }

        //
        // POST: /Institute/Create

        [HttpPost]
        public ActionResult Create(Institute institute)
        {
            if (ModelState.IsValid)
            {
                db.Institutes.Add(institute);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CityId = new SelectList(db.City, "Id", "CityName", institute.CityId);
            return View(institute);
        }

        //
        // GET: /Institute/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Institute institute = db.Institutes.Find(id);
            if (institute == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(db.City, "Id", "CityName", institute.CityId);
            return View(institute);
        }

        //
        // POST: /Institute/Edit/5

        [HttpPost]
        public ActionResult Edit(Institute institute)
        {
            if (ModelState.IsValid)
            {
                db.Entry(institute).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CityId = new SelectList(db.City, "Id", "CityName", institute.CityId);
            return View(institute);
        }

        //
        // GET: /Institute/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Institute institute = db.Institutes.Find(id);
            if (institute == null)
            {
                return HttpNotFound();
            }
            return View(institute);
        }

        //
        // POST: /Institute/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Institute institute = db.Institutes.Find(id);
            db.Institutes.Remove(institute);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}