﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLA.Models;
using APLA.DAL;
using System.Web.Configuration;
using PagedList;

namespace APLA.Controllers
{
    public class SubjectController : Controller
    {
        private AplaEntities db = new AplaEntities();

        //
        // GET: /Subject/

        public ActionResult Index(string currentFilter, string searchString, int? page)
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var subjects = from t in db.Subjects
                               select t;

            if (!String.IsNullOrEmpty(searchString))
            {
                subjects = subjects.Where(t => t.Name.ToLower().Contains(searchString.ToLower()));
            }

            var result = WebConfigurationManager.AppSettings["SubjectListPageSize"];
            int pageSize = 5;
            if (!string.IsNullOrEmpty(result))
            {
                pageSize = Convert.ToInt32(result);
            }

            int pageNumber = (page ?? 1);

            return View(subjects.ToList().ToPagedList(pageNumber, pageSize));
        }

        //
        // GET: /Subject/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    Subject subject = db.Subjects.Find(id);
        //    if (subject == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(subject);
        //}

        //
        // GET: /Subject/Create

        public ActionResult Create()
        {
            ViewBag.EducationStreamId = new SelectList(db.EducationStreams, "Id", "Name");
            return View();
        }

        //
        // POST: /Subject/Create

        [HttpPost]
        public ActionResult Create(Subject subject)
        {
            if (ModelState.IsValid)
            {
                db.Subjects.Add(subject);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EducationStreamId = new SelectList(db.EducationStreams, "Id", "Name", subject.EducationStreamId);
            return View(subject);
        }

        //
        // GET: /Subject/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Subject subject = db.Subjects.Find(id);
            if (subject == null)
            {
                return HttpNotFound();
            }
            ViewBag.EducationStreamId = new SelectList(db.EducationStreams, "Id", "Name", subject.EducationStreamId);
            return View(subject);
        }

        //
        // POST: /Subject/Edit/5

        [HttpPost]
        public ActionResult Edit(Subject subject)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subject).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EducationStreamId = new SelectList(db.EducationStreams, "Id", "Name", subject.EducationStreamId);
            return View(subject);
        }

        //
        // GET: /Subject/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Subject subject = db.Subjects.Find(id);
            if (subject == null)
            {
                return HttpNotFound();
            }
            return View(subject);
        }

        //
        // POST: /Subject/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Subject subject = db.Subjects.Find(id);
            db.Subjects.Remove(subject);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}