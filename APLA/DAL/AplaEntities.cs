﻿using APLA.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace APLA.DAL
{
    public class AplaEntities : DbContext
    {
        public AplaEntities()
            : base("AplaConnection")
        {

        }

        public DbSet<Children> Children { get; set; }
        public DbSet<Institute> Institutes { get; set; }
        public DbSet<Occupation> Occupations { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<City> City { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<Race> Races { get; set; }
        public DbSet<Religion> Religions { get; set; }
        public DbSet<EducationStream> EducationStreams { get; set; }
        public DbSet<TimeTable> TimeTables { get; set; }
        public DbSet<ClassType> ClassTypes { get; set; }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UsersInRole> UsersInRoles { get; set; }

    }
}