﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace APLA.Models
{
    [Table("Apla_District")]
    public class District
    {
        public District()
        {
            this.City = new HashSet<City>();
        }

        public int Id { get; set; }
        public string DistrictName { get; set; }

        public virtual ICollection<City> City { get; set; }
    }
}