﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APLA.Common
{
    public enum UserRoles
    {
        SuperAdmin,

        Admin,

        Internal,
    }
}