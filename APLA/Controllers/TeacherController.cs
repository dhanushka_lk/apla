﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLA.Models;
using APLA.DAL;
using WebMatrix.WebData;
using System.Web.Security;
using APLA.Common;
using APLA.Helpers;
using System.Collections;
using System.IO;
using PagedList;
using System.Web.Configuration;
using Elmah;

namespace APLA.Controllers
{
    [Authorize(Roles="SuperAdmin,Admin")]
    public class TeacherController : Controller
    {
        private AplaEntities db = new AplaEntities();

        //
        // GET: /Teacher/

        public ActionResult Index()
        {
            try
            {
                string page = string.Empty;
                var queryString = Request.QueryString.GetValues("grid-page");
                if (queryString != null && queryString.Count() > 0)
                {
                    page = queryString.FirstOrDefault();
                    ViewBag.Page = page;
                }

                var teachers = from t in db.Teachers
                               select t;
                return View(teachers.ToList());
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                WebSecurity.Logout();
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // GET: /Teacher/Details/5

        public ActionResult Details(int id = 0,int page=0)
        {
            try
            {
                ViewBag.Page = page;
                Teacher teacher = db.Teachers.Find(id);
                if (teacher == null)
                {
                    return HttpNotFound();
                }

                var currentDistrictResult = db.Districts.Where(d => d.Id == teacher.CurrentDistrictId).FirstOrDefault();
                string currentDistrict = string.Empty;
                if (currentDistrictResult != null)
                {
                    currentDistrict = currentDistrictResult.DistrictName;
                }

                var currentCityResult = db.City.Where(d => d.Id == teacher.CurrentCityId).FirstOrDefault();
                string currentCity = string.Empty;
                if (currentCityResult != null)
                {
                    currentCity = currentCityResult.CityName;
                }

                var permanentDistrictResult = db.Districts.Where(d => d.Id == teacher.PermanentDistrictId).FirstOrDefault();
                string permanentDistrict = string.Empty;
                if (permanentDistrictResult != null)
                {
                    permanentDistrict = permanentDistrictResult.DistrictName;
                }

                var permanentCityResult = db.City.Where(d => d.Id == teacher.PermanentCityId).FirstOrDefault();
                string permanentCity = string.Empty;
                if (permanentCityResult != null)
                {
                    permanentCity = permanentCityResult.CityName;
                }

                ViewBag.CurrentAddress = string.Format("{0},{1},{2}", teacher.CurrentLane, currentCity, currentDistrict);
                ViewBag.permanentAddress = string.Format("{0},{1},{2}", teacher.PermanentLane, permanentCity, permanentDistrict);
                ViewBag.Name = string.Format("{0} {1} {2}", teacher.Name, teacher.CommonName, teacher.FullName);

                ViewBag.UserName = db.Users.Where(u => u.UserId == teacher.UserId).SingleOrDefault().UserName;

                return View(teacher);
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /Teacher/Details/5

        public ActionResult IdForm(int id = 0)
        {
            try
            {
                Teacher teacher = db.Teachers.Find(id);
                if (teacher == null)
                {
                    return HttpNotFound();
                }

                var permanentDistrict = db.Districts.Where(d => d.Id == teacher.PermanentDistrictId).FirstOrDefault().DistrictName;
                var permanentCity = db.City.Where(d => d.Id == teacher.PermanentCityId).FirstOrDefault().CityName;
                ViewBag.permanentAddress = string.Format("{0},{1},{2}", teacher.PermanentLane, permanentCity, permanentDistrict);
                ViewBag.Name = string.Format("{0} {1} {2}", teacher.Name, teacher.CommonName, teacher.FullName);

                return View(teacher);
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /Teacher/Create

        public ActionResult Create()
        {
            try
            {
                string fmt = "00000.##";
                string NewReg = string.Empty;
                string reg = string.Empty;
                int maxRegNo = 0;
                List<int> numbers = new List<int>();
                int newRegNo = 0;

                var teacherList = db.Teachers;
                if (teacherList != null && teacherList.Count() > 0)
                {
                    foreach (Teacher t in teacherList.ToList())
                    {
                        reg = t.RegNo;
                        int regNo = 0;
                        if (!string.IsNullOrEmpty(reg))
                        {
                            var array = reg.Split('_');
                            regNo = Convert.ToInt32(array[2]);
                            numbers.Add(regNo);
                        }
                    }

                    var max = db.Teachers.OrderByDescending(t => t.RegNo).FirstOrDefault();
                    if (max != null)
                    {
                        var array = max.RegNo.Split('_');
                        maxRegNo = Convert.ToInt32(array[2]);
                    }

                    var result = Enumerable.Range(1, maxRegNo).Except(numbers);
                    if (result != null && result.Count() > 0)
                    {
                        newRegNo = result.FirstOrDefault();
                    }
                    else
                    {
                        newRegNo = maxRegNo + 1;
                    }
                }
                else
                {
                    newRegNo = 1;
                }

                NewReg = string.Format("Apla_{0}_{1}", DateTime.Now.Year, newRegNo.ToString(fmt));

                ViewBag.CityId = new SelectList(db.City, "Id", "CityName");
                ViewBag.DistrictId = new SelectList(db.Districts, "Id", "DistrictName");
                ViewBag.RaceId = new SelectList(db.Races, "Id", "Name");
                ViewBag.ReligionId = new SelectList(db.Religions, "Id", "Name");

                ViewBag.EducationStreams = db.EducationStreams.ToList();
                ViewBag.Institutes = db.Institutes.ToList();
                ViewBag.Occupations = db.Occupations.ToList();
                ViewBag.Reg = NewReg;
                ViewBag.Names = new SelectList((IEnumerable)SelectListItemHelper.GetPersonDetails(), "Text", "Value");
                return View();
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                return RedirectToAction("Index");
            }
        }

        //
        // POST: /Teacher/Create

        [HttpPost]
        public ActionResult Create(Teacher teacher, HttpPostedFileBase file)
        {
            try
            {
                bool validationResult = false;
                var keys = Request.Form.AllKeys.ToList();

                foreach (var key in keys)
                {
                    if (key.ToString().StartsWith("s"))
                    {
                        validationResult = true;
                    }
                }

                int iFileSize = 0;

                if (file != null)
                {
                    iFileSize = file.ContentLength;
                }

                if (iFileSize > 5000000)  // 1MB approx (actually less though)
                {
                    // File is too big so do something here
                    ViewBag.Error = "File exceeds the maximum file size 5MB";
                }

                string userName = string.Empty;

                try
                {

                    if (ModelState.IsValid && validationResult && iFileSize < 5000000)
                    {
                        userName = Request["username"];
                        var password = Request["password"];
                        WebSecurity.CreateUserAndAccount(userName, password);
                        var roles = (SimpleRoleProvider)Roles.Provider;
                        if (!roles.RoleExists(Common.UserRoles.Internal.ToString()))
                        {
                            roles.CreateRole(Common.UserRoles.Internal.ToString());
                        }

                        Roles.AddUserToRole(userName, UserRoles.Internal.ToString());

                        int userId = WebSecurity.GetUserId(userName);
                        teacher.UserId = userId;


                        if (file != null)
                        {
                            string pic = System.IO.Path.GetExtension(file.FileName);
                            string path = Server.MapPath("~/Images/ProfileImages/" + teacher.RegNo + pic);
                            file.SaveAs(path);
                        }

                        //adding education streams
                        ICollection<EducationStream> educationStreamList = new HashSet<EducationStream>();
                        var educationStreams = db.EducationStreams.ToList();
                        foreach (EducationStream stream in educationStreams)
                        {
                            var result = Request[Convert.ToString("c" + stream.Id)];
                            if (result != null)
                            {
                                if (result.ToString() == "on")
                                {
                                    educationStreamList.Add(stream);
                                }
                            }
                        }

                        teacher.EducationStreams = educationStreamList;

                        //adding subjects
                        ICollection<Subject> subjectList = new HashSet<Subject>();
                        var subjects = db.Subjects.ToList();
                        foreach (Subject subject in subjects)
                        {
                            var result = Request[Convert.ToString("s" + subject.Id)];
                            if (result != null)
                            {
                                if (result.ToString() == "on")
                                {
                                    subjectList.Add(subject);
                                }
                            }
                        }

                        teacher.Subjects = subjectList;

                        var instituteResult = Request["Ins"];
                        var cityResult = Request["Ins_city"];
                        ICollection<Institute> instituteList = new HashSet<Institute>();

                        if (!string.IsNullOrEmpty(instituteResult))
                        {
                            var instituteListResult = instituteResult.Split(',').ToList();
                            var cityListResult = cityResult.Split(',').ToList();

                            for (int z = 0; z < instituteListResult.Count; z++)
                            {
                                bool createNew = true;
                                string insName = instituteListResult[z];
                                var foundInstitute = db.Institutes.Where(i => i.Name == insName).FirstOrDefault();
                                if (foundInstitute != null)
                                {
                                    if (foundInstitute.CityId == Convert.ToInt32(cityListResult[z]))
                                    {
                                        createNew = false;
                                    }
                                }

                                if (createNew)
                                {
                                    Institute institute = new Institute
                                    {
                                        Name = instituteListResult[z],
                                        CityId = Convert.ToInt32(cityListResult[z])
                                    };
                                    db.Institutes.Add(institute);
                                    db.SaveChanges();
                                    instituteList.Add(institute);
                                }
                                else
                                {
                                    instituteList.Add(foundInstitute);
                                }
                            }
                        }

                        teacher.Institutes = instituteList;


                        //adding Occupations
                        ICollection<Occupation> OccupationList = new HashSet<Occupation>();
                        var occupations = db.Occupations.ToList();
                        foreach (Occupation occupation in occupations)
                        {
                            var result = Request[Convert.ToString("o" + occupation.Id)];
                            if (result != null)
                            {
                                if (result.ToString() == "on")
                                {
                                    OccupationList.Add(occupation);
                                }
                            }
                        }

                        teacher.Occupations = OccupationList;
                        teacher.Enabled = true;
                        teacher.Approved = true;

                        db.Teachers.Add(teacher);
                        db.SaveChanges();

                        ICollection<Children> ChildrenList = new HashSet<Children>();

                        var names = Request["names"];
                        if (!string.IsNullOrEmpty(names))
                        {
                            var bdays = Request["bdays"];
                            var nameList = names.Split(',').ToList();
                            var bdayList = bdays.Split(',').ToList();

                            for (int x = 0; x < nameList.Count(); x++)
                            {
                                Children child = new Children();
                                child.Name = Convert.ToString(nameList[x]);
                                if (bdayList[x] != null)
                                {
                                    try
                                    {
                                        child.Bday = Convert.ToDateTime(bdayList[x]);
                                    }
                                    catch (Exception)
                                    {
                                        child.Bday = DateTime.Now;
                                    }
                                }
                                else { child.Bday = DateTime.Now; }
                                child.TeacherId = teacher.Id;
                                db.Children.Add(child);
                                db.SaveChanges();
                            }

                        }
                    }
                }
                catch (Exception)
                {
                    int userId = WebSecurity.GetUserId(userName);
                    User user = db.Users.Find(userId);
                    db.Users.Remove(user);
                    db.SaveChanges();
                    //ViewBag.Error = e.InnerException.Message+""+e.StackTrace+" "+e.InnerException.InnerException.Message;
                }


                string fmt = "00000.##";
                string NewReg = string.Empty;
                string reg = string.Empty;
                int maxRegNo = 0;
                List<int> numbers = new List<int>();
                int newRegNo = 0;

                var teacherList = db.Teachers;
                if (teacherList != null && teacherList.Count() > 0)
                {
                    foreach (Teacher t in teacherList.ToList())
                    {
                        reg = t.RegNo;
                        int regNo = 0;
                        if (!string.IsNullOrEmpty(reg))
                        {
                            var array = reg.Split('_');
                            regNo = Convert.ToInt32(array[2]);
                            numbers.Add(regNo);
                        }
                    }

                    var max = db.Teachers.OrderByDescending(t => t.RegNo).FirstOrDefault();
                    if (max != null)
                    {
                        var array = max.RegNo.Split('_');
                        maxRegNo = Convert.ToInt32(array[2]);
                    }

                    var result = Enumerable.Range(1, maxRegNo).Except(numbers);
                    if (result != null && result.Count() > 0)
                    {
                        newRegNo = result.FirstOrDefault();
                    }
                    else
                    {
                        newRegNo = maxRegNo + 1;
                    }
                }
                else
                {
                    newRegNo = 1;
                }

                NewReg = string.Format("Apla_{0}_{1}", DateTime.Now.Year, newRegNo.ToString(fmt));


                ViewBag.CityId = new SelectList(db.City, "Id", "CityName");
                ViewBag.DistrictId = new SelectList(db.Districts, "Id", "DistrictName");
                ViewBag.RaceId = new SelectList(db.Races, "Id", "Name");
                ViewBag.ReligionId = new SelectList(db.Religions, "Id", "Name");

                ViewBag.EducationStreams = db.EducationStreams.ToList();
                ViewBag.Institutes = db.Institutes.ToList();
                ViewBag.Occupations = db.Occupations.ToList();
                ViewBag.Reg = NewReg;
                ViewBag.Names = new SelectList((IEnumerable)SelectListItemHelper.GetPersonDetails(), "Text", "Value");
                if (!validationResult)
                {
                    ViewBag.Error = "Please select atleast one subject";
                }
                return View(teacher);
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /Teacher/Edit/5

        public ActionResult Edit(int id = 0,int page=0)
        {
            try
            {
                ViewBag.Page = page;
                Teacher teacher = db.Teachers.Find(id);
                if (teacher == null)
                {
                    return HttpNotFound();
                }
                ViewBag.CurrentCity = new SelectList(db.City, "Id", "CityName", teacher.CurrentCityId);
                ViewBag.CurrentDistrict = new SelectList(db.Districts, "Id", "DistrictName", teacher.CurrentDistrictId);
                ViewBag.PermanentCity = new SelectList(db.City, "Id", "CityName", teacher.PermanentCityId);
                ViewBag.PermanentDistrict = new SelectList(db.Districts, "Id", "DistrictName", teacher.PermanentDistrictId);
                ViewBag.Race = new SelectList(db.Races, "Id", "Name", teacher.RaceId);
                ViewBag.Religion = new SelectList(db.Religions, "Id", "Name", teacher.ReligionId);
                ViewBag.EducationStreams = db.EducationStreams.ToList();
                ViewBag.Institutes = db.Institutes.ToList();
                ViewBag.Occupations = db.Occupations.ToList();
                ViewBag.Subjects = db.Subjects.ToList();
                ViewBag.Names = new SelectList((IEnumerable)SelectListItemHelper.GetPersonDetails(), "Text", "Value");

                ViewBag.UserName = db.Users.Where(u => u.UserId == teacher.UserId).SingleOrDefault().UserName;
                return View(teacher);
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                return RedirectToAction("Index");
            }
        }

        //
        // POST: /Teacher/Edit/5

        [HttpPost]
        public ActionResult Edit(Teacher teacher, string[] selectedEducationStreams, string[] selectedOccupations, string[] selectedSubjects, HttpPostedFileBase file, int page = 0)
        {
            try
            {
                ViewBag.Page = page;

                int iFileSize = 0;

                if (file != null)
                {
                    iFileSize = file.ContentLength;
                }

                if (iFileSize > 5000000)  // 1MB approx (actually less though)
                {
                    // File is too big so do something here
                    ViewBag.Error = "File exceeds the maximum file size 5MB";
                }

                var userName = Request["username"];
                if (string.IsNullOrEmpty(userName))
                {
                    ModelState.AddModelError("UserName", "Username cannot be empty ");
                }
                else
                {
                    foreach (var item in db.Users.Where(u => u.UserName == userName).ToList())
                    {
                        if (item.UserId != teacher.UserId)
                        {
                            ModelState.AddModelError("UserName", "Username Already in use");
                        }
                    }
                }

                if (ModelState.IsValid && iFileSize < 5000000)
                {
                    db.Entry(teacher).State = EntityState.Modified;
                    db.SaveChanges();

                    var user = db.Users.Where(u => u.UserId == teacher.UserId).SingleOrDefault();
                    user.UserName = userName;
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();

                    if (file != null)
                    {
                        try
                        {
                            var imgPath = Directory.EnumerateFiles(Server.MapPath(@"~/Images/ProfileImages/"), "*.*", SearchOption.AllDirectories).Where(s => s.Contains(teacher.RegNo));
                            if (imgPath != null && imgPath.Count() > 0)
                            {
                                foreach (var filePath in imgPath)
                                {
                                    System.IO.File.Delete(filePath);
                                }
                            }
                        }
                        catch (Exception)
                        {

                        }

                        string pic = System.IO.Path.GetExtension(file.FileName);
                        string path = Server.MapPath("~/Images/ProfileImages/" + teacher.RegNo + pic);
                        file.SaveAs(path);
                    }

                    if (selectedEducationStreams != null)
                    {
                        ICollection<EducationStream> educationStreamList = new HashSet<EducationStream>();
                        foreach (var educationStream in db.EducationStreams.ToList())
                        {
                            if (selectedEducationStreams.ToList().Contains(educationStream.Id.ToString()))
                            {
                                educationStream.Teachers.Add(teacher);
                                educationStreamList.Add(educationStream);
                            }
                            else
                            {
                                educationStream.Teachers.Remove(teacher);
                            }
                        }

                        teacher.EducationStreams = educationStreamList;
                    }

                    if (selectedSubjects != null)
                    {
                        //adding subjects
                        ICollection<Subject> subjectList = new HashSet<Subject>();
                        foreach (var subject in db.Subjects.ToList())
                        {
                            if (selectedSubjects.ToList().Contains(subject.Id.ToString()))
                            {
                                subject.Teachers.Add(teacher);
                                subjectList.Add(subject);
                            }
                            else
                            {
                                subject.Teachers.Remove(teacher);
                            }
                        }

                        teacher.Subjects = subjectList;

                    }

                    var instituteResult = Request["Ins"];
                    var cityResult = Request["Ins_city"];
                    ICollection<Institute> instituteList = new HashSet<Institute>();

                    if (instituteResult != null)
                    {
                        var instituteListResult = instituteResult.Split(',').ToList();
                        var cityListResult = cityResult.Split(',').ToList();

                        foreach (var institute in db.Institutes.ToList())
                        {
                            if (institute.Teachers.Contains(teacher))
                            {
                                institute.Teachers.Remove(teacher);
                            }
                        }

                        for (int z = 0; z < instituteListResult.Count; z++)
                        {
                            bool createNew = true;
                            string insName = instituteListResult[z];
                            Institute selectedInstitute = null;
                            var foundInstitutes = db.Institutes.Where(i => i.Name == insName);
                            if (foundInstitutes != null)
                            {
                                foreach (var foundInstitute in foundInstitutes.ToList())
                                {
                                    if (foundInstitute.City.CityName == cityListResult[z].ToString())
                                    {
                                        createNew = false;
                                        selectedInstitute = foundInstitute;
                                    }
                                }
                            }

                            if (createNew)
                            {
                                int cityId = 1;
                                string cityName = cityListResult[z].ToString();
                                var city = db.City.Where(c => c.CityName == cityName).FirstOrDefault();
                                if (city != null)
                                {
                                    cityId = city.Id;
                                }
                                Institute institute = new Institute
                                {
                                    Name = instituteListResult[z],
                                    CityId = cityId
                                };

                                institute.Teachers.Add(teacher);
                                db.Institutes.Add(institute);
                                db.SaveChanges();
                                instituteList.Add(institute);
                            }
                            else
                            {
                                selectedInstitute.Teachers.Add(teacher);
                                instituteList.Add(selectedInstitute);
                            }
                        }
                    }

                    teacher.Institutes = instituteList;

                    if (selectedOccupations != null)
                    {

                        //adding Occupations
                        ICollection<Occupation> OccupationList = new HashSet<Occupation>();
                        foreach (var occupation in db.Occupations.ToList())
                        {
                            if (selectedOccupations.ToList().Contains(occupation.Id.ToString()))
                            {
                                occupation.Teachers.Add(teacher);
                                OccupationList.Add(occupation);
                            }
                            else
                            {
                                occupation.Teachers.Remove(teacher);
                            }
                        }

                        teacher.Occupations = OccupationList;
                    }

                    var names = Request["names"];
                    var bdays = Request["bdays"];

                    string[] nameArray = null;
                    string[] bdayArray = null;

                    if (!string.IsNullOrEmpty(names) && bdays != null)
                    {
                        nameArray = names.Split(',');
                        bdayArray = bdays.Split(',');

                        if (nameArray.Count() > 0)
                        {
                            var childList = db.Children.Where(c => c.TeacherId == teacher.Id).ToList();

                            foreach (var item in childList)
                            {
                                db.Children.Remove(item);
                                db.SaveChanges();
                            }

                            for (int x = 0; x < nameArray.Count(); x++)
                            {
                                Children child = new Children();
                                child.Name = Convert.ToString(nameArray[x]);
                                child.Bday = Convert.ToDateTime(bdayArray[x]);
                                child.TeacherId = teacher.Id;
                                db.Children.Add(child);
                                db.SaveChanges();
                            }
                        }
                    }

                    db.Entry(teacher).State = EntityState.Modified;
                    db.SaveChanges();
                    //return RedirectToAction("Index", new { grid-page = page });
                    return Redirect("/Teacher/Index?grid-page=" + page);
                }

                ViewBag.Race = new SelectList(db.Races, "Id", "Name", teacher.RaceId);
                ViewBag.Religion = new SelectList(db.Religions, "Id", "Name", teacher.ReligionId);
                ViewBag.CurrentCity = new SelectList(db.City, "Id", "CityName", db.City.Where(c => c.Id == teacher.CurrentCityId).SingleOrDefault().Id);
                ViewBag.CurrentDistrict = new SelectList(db.Districts, "Id", "DistrictName", db.Districts.Where(d => d.Id == teacher.CurrentDistrictId).SingleOrDefault().Id);
                ViewBag.PermanentCity = new SelectList(db.City, "Id", "CityName", teacher.PermanentCityId);
                ViewBag.PermanentDistrict = new SelectList(db.Districts, "Id", "DistrictName", teacher.PermanentDistrictId);
                ViewBag.EducationStreams = db.EducationStreams.ToList();
                ViewBag.Institutes = db.Institutes.ToList();
                ViewBag.Occupations = db.Occupations.ToList();
                ViewBag.Subjects = db.Subjects.ToList();
                ViewBag.Names = new SelectList((IEnumerable)SelectListItemHelper.GetPersonDetails(), "Text", "Value");
                return View(teacher);
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /Teacher/Delete/5

        public ActionResult Delete(int id = 0, int page = 0)
        {
            try
            {
                ViewBag.Page = page;
                Teacher teacher = db.Teachers.Find(id);
                if (teacher == null)
                {
                    return HttpNotFound();
                }
                return View(teacher);
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                return RedirectToAction("Index");
            }
        }

        //
        // POST: /Teacher/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id, int page = 0)
        {
            try
            {
                Teacher teacher = db.Teachers.Find(id);
                teacher.Enabled = false;
                db.Entry(teacher).State = EntityState.Modified;
                db.SaveChanges();
                //Teacher teacher = db.Teachers.Find(id);
                //db.Teachers.Remove(teacher);
                //db.SaveChanges();
                //var user = db.Users.Where(u => u.UserId == teacher.UserId).SingleOrDefault();
                //System.Web.Security.Membership.DeleteUser(user.UserName);

                //var userRoles = db.UsersInRoles.Where(u=>u.UserId==user.UserId).ToList();

                //foreach (var item in userRoles)
                //{
                //    db.UsersInRoles.Remove(item);
                //}

                //return RedirectToAction("Index", new { grid-page = page });
                return Redirect("/Teacher/Index?grid-page=" + page);
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult GetCityByDisId(int districtid)
        {
            List<City> objcity = db.City.Where(c => c.DistrictId == districtid).ToList();
            SelectList obgcity = new SelectList(objcity, "Id", "CityName", 0);
            return Json(obgcity, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckValidUserName(string userName)
        {
            var result = db.Users.Where(u => u.UserName == userName).SingleOrDefault();
            if (result != null)
            {
                return Json(new { valid = false }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { valid = true }, JsonRequestBehavior.AllowGet);

            }
        }

        public JsonResult AutoCompleteInstitute(string term)
        {
            var result = (from r in db.Institutes
                          where r.Name.ToLower().Contains(term.ToLower())
                          select new { r.Name }).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AutoCompleteCity(string term)
        {
            var result = (from r in db.City
                          where r.CityName.ToLower().Contains(term.ToLower())
                          select new { r.CityName }).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCityForInstitute(string term)
        {
            var result = (from r in db.Institutes
                          where r.Name.ToLower().Contains(term.ToLower())
                          select new { r.City.CityName }).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChangePassword(int id = 0, int page = 0)
        {
            ViewBag.Page = page;
            var teacher=db.Teachers.Find(id);
            if (teacher != null)
            {
                var result = db.Users.Where(u => u.UserId == teacher.UserId).SingleOrDefault();
                if (result != null)
                {
                    string userName = result.UserName;
                    ViewBag.UserName = userName;
                    return View();
                }
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult ChangePassword(LocalPasswordModel model, int page = 0)
        {
            try
            {
                var result = Request["UserName"];
                if (!string.IsNullOrEmpty(result))
                {
                    var token = WebSecurity.GeneratePasswordResetToken(result);
                    WebSecurity.ResetPassword(token, model.NewPassword);
                    //return RedirectToAction("Index",new { grid-page = page });
                    return Redirect("/Teacher/Index?grid-page=" + page);
                }
            }
            catch (Exception)
            {

            }

            return RedirectToAction("Index");
        }
    }
}