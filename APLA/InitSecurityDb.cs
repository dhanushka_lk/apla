﻿using APLA.Common;
using APLA.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using WebMatrix.WebData;

namespace APLA
{
    public class InitSecurityDb : DropCreateDatabaseIfModelChanges<AplaEntities>
    {
        protected override void Seed(AplaEntities context)
        {
            WebSecurity.InitializeDatabaseConnection("AplaConnection", "Apla_User", "UserId", "UserName", autoCreateTables: true);
            var roles = (SimpleRoleProvider)Roles.Provider;
            var membership = (SimpleMembershipProvider)Membership.Provider;

            if (!roles.RoleExists(UserRoles.SuperAdmin.ToString()))
            {
                roles.CreateRole(Common.UserRoles.SuperAdmin.ToString());
            }

            if (!roles.RoleExists(Common.UserRoles.Admin.ToString()))
            {
                roles.CreateRole(Common.UserRoles.Admin.ToString());
            }

            if (!roles.RoleExists(Common.UserRoles.Internal.ToString()))
            {
                roles.CreateRole(Common.UserRoles.Internal.ToString());
            }

            if (membership.GetUser("SuperAdmin", false) == null)
            {
                membership.CreateUserAndAccount("SuperAdmin", "123456");
            }

            if (!roles.GetRolesForUser("SuperAdmin").Contains("SuperAdmin"))
            {
                roles.AddUsersToRoles(new[] { "SuperAdmin" }, new[] { "SuperAdmin" });
            }

            var baseDir = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin", string.Empty);

            context.Database.ExecuteSqlCommand(File.ReadAllText(baseDir + "\\InitialData.sql"));

            context.SaveChanges();
        }
    }
}

