﻿using APLA.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace APLA.Controllers
{
    public class HomeController : Controller
    {
        private AplaEntities db = new AplaEntities();

        public ActionResult Index()
        {
            if (User.IsInRole("Admin") || User.IsInRole("SuperAdmin"))
            {
                return RedirectToAction("Index", "Teacher");
            }
            if (User.IsInRole("Internal"))
            {
                return RedirectToAction("Details", "PublicTeacher");
            }

            ViewBag.CityId = new SelectList(db.City, "Id", "CityName");
            ViewBag.DistrictId = new SelectList(db.Districts, "Id", "DistrictName");
            ViewBag.EducationStreamId = new SelectList(db.EducationStreams, "Id", "Name");
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Name");
            ViewBag.ClassTypeId = new SelectList(db.ClassTypes, "Id", "Name");

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
