﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLA.Models;
using APLA.DAL;
using APLA.Helpers;
using System.Collections;

namespace APLA.Controllers
{
    [Authorize(Roles="Internal")]
    public class TimeTableController : Controller
    {
        private AplaEntities db = new AplaEntities();

        //
        // GET: /TimeTable/
        public ActionResult Index()
        {
            int userId = db.Users.Where(u => u.UserName == User.Identity.Name).SingleOrDefault().UserId;
            int teacherId=db.Teachers.Where(t=>t.UserId==userId).SingleOrDefault().Id;
            var timetables = db.TimeTables.Where(t => t.TeacherId == teacherId).ToList();
            return View(timetables.ToList());
        }

        //
        // GET: /TimeTable/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    TimeTable timetable = db.TimeTables.Find(id);
        //    if (timetable == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(timetable);
        //}

        //
        // GET: /TimeTable/Create

        public ActionResult Create()
        {
            ViewBag.Days = new SelectList((IEnumerable)SelectListItemHelper.Getdays(), "Text", "Value");
            ViewBag.Years = new SelectList((IEnumerable)SelectListItemHelper.GetYears(), "Text", "Value");
            ViewBag.EducationStreamId = new SelectList(db.EducationStreams, "Id", "Name");
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Name");
            ViewBag.ClassTypeId = new SelectList(db.ClassTypes, "Id", "Name");
            return View();
        }

        //
        // POST: /TimeTable/Create

        [HttpPost]
        public ActionResult Create(TimeTable timetable)
        {
            if (ModelState.IsValid)
            {
                int userId = db.Users.Where(u => u.UserName == User.Identity.Name).SingleOrDefault().UserId;
                int teacherId = db.Teachers.Where(t => t.UserId == userId).SingleOrDefault().Id;
                timetable.TeacherId = teacherId;
                db.TimeTables.Add(timetable);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Days = new SelectList((IEnumerable)SelectListItemHelper.Getdays(), "Text", "Value");
            ViewBag.Years = new SelectList((IEnumerable)SelectListItemHelper.GetYears(), "Text", "Value");
            ViewBag.EducationStreamId = new SelectList(db.EducationStreams, "Id", "Name", timetable.EducationStreamId);
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Name", timetable.SubjectId);
            ViewBag.ClassTypeId = new SelectList(db.ClassTypes, "Id", "Name", timetable.ClassTypeId);
            return View(timetable);
        }

        
         //GET: /TimeTable/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TimeTable timetable = db.TimeTables.Find(id);
            if (timetable == null)
            {
                return HttpNotFound();
            }
            ViewBag.EducationStreamId = new SelectList(db.EducationStreams, "Id", "Name", db.EducationStreams.Where(e=>e.Id==timetable.EducationStreamId).SingleOrDefault().Id);
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Name", timetable.SubjectId);
            ViewBag.ClassTypeId = new SelectList(db.ClassTypes, "Id", "Name", timetable.ClassTypeId);
            ViewBag.Days = new SelectList((IEnumerable)SelectListItemHelper.Getdays(), "Text", "Value");
            ViewBag.Years = new SelectList((IEnumerable)SelectListItemHelper.GetYears(), "Text", "Value");
            return View(timetable);
        }

        //
        // POST: /TimeTable/Edit/5

        [HttpPost]
        public ActionResult Edit(TimeTable timetable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(timetable).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EducationStreamId = new SelectList(db.EducationStreams, "Id", "Name", db.EducationStreams.Where(e => e.Id == timetable.EducationStreamId).SingleOrDefault().Id);
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Name", timetable.SubjectId);
            ViewBag.ClassTypeId = new SelectList(db.ClassTypes, "Id", "Name", timetable.ClassTypeId);
            ViewBag.Days = new SelectList((IEnumerable)SelectListItemHelper.Getdays(), "Text", "Value");
            ViewBag.Years = new SelectList((IEnumerable)SelectListItemHelper.GetYears(), "Text", "Value");
            return View(timetable);
        }

        //
        // GET: /TimeTable/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TimeTable timetable = db.TimeTables.Find(id);
            if (timetable == null)
            {
                return HttpNotFound();
            }
            return View(timetable);
        }

        //
        // POST: /TimeTable/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TimeTable timetable = db.TimeTables.Find(id);
            db.TimeTables.Remove(timetable);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult GetSubjects(int sreamId)
        {
            List<Subject> objcity = db.Subjects.Where(s=>s.EducationStreamId==sreamId).ToList();
            SelectList obgsubject = new SelectList(objcity, "Id", "Name", 0);
            return Json(obgsubject, JsonRequestBehavior.AllowGet);
        }
    }
}