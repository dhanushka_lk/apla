﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace APLA.Models
{
    [Table("Apla_Children")]
    public class Children
    {
        public Children()
        {

        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Bday { get; set; }
        public int TeacherId { get; set; }

        public virtual Teacher Teacher { get; set; }
    }
}