﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace APLA.Helpers
{
    public class SelectListItemHelper
    {
        public static IEnumerable<SelectListItem> Getdays()
        {
            IList<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem{Text = "Monday", Value = "Monday"},
                new SelectListItem{Text = "Tuesday", Value = "Tuesday"},
                new SelectListItem{Text = "Wednesday", Value = "Wednesday"},
                new SelectListItem{Text = "Thrusday", Value = "Thrusday"},
                new SelectListItem{Text = "Friday", Value = "Friday"},
                new SelectListItem{Text = "Saturday", Value = "Saturday"},
                new SelectListItem{Text = "Sunday", Value = "Sunday"}
            };
            return items;
        }

        public static IEnumerable<SelectListItem> GetPersonDetails()
        {
            IList<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem{Text = "Mr", Value = "Mr"},
                new SelectListItem{Text = "Mrs", Value = "Mrs"},
                new SelectListItem{Text = "Miss", Value = "Miss"},
                new SelectListItem{Text = "Ven", Value = "Ven"},
                new SelectListItem{Text = "Rev", Value = "Rev"},
            };
            return items;
        }

        public static IEnumerable<SelectListItem> GetYears()
        {
            IList<SelectListItem> items = new List<SelectListItem>();

            for (int x = 2013; x < 2020; x++)
            {
                items.Add(new SelectListItem { Text = x.ToString(), Value = x.ToString() });
            }

            return items;
        }
    }
}