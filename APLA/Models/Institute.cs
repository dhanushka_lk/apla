﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace APLA.Models
{
    [Table("Apla_Institute")]
    public class Institute
    {
        public Institute()
        {
            this.Teachers = new HashSet<Teacher>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int CityId { get; set; }

        public virtual ICollection<Teacher> Teachers { get; set; }
        public virtual City City { get; set; }

    }
}