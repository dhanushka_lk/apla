﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace APLA.Models
{
    [Table("Apla_Subject")]
    public class Subject
    {
        public Subject()
        {
            this.Teachers = new HashSet<Teacher>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int EducationStreamId { get; set; }

        public virtual ICollection<Teacher> Teachers { get; set; }
        public virtual EducationStream EducationStream { get; set; }
    }
}