﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace APLA.Models
{
    [Table("Apla_EducationStream")]
    public class EducationStream
    {
        public EducationStream()
        {
            this.Subjects = new HashSet<Subject>();
            this.Teachers = new HashSet<Teacher>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Subject> Subjects { get; set; }
        public virtual ICollection<Teacher> Teachers { get; set; }

    }
}