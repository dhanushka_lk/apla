﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLA.Models;
using APLA.DAL;
using WebMatrix.WebData;
using System.Web.Security;
using APLA.Common;
using System.Collections;
using APLA.Helpers;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using PagedList;
using System.Web.Configuration;
using Elmah;
using System.Net.Mail;
using System.Text;

namespace APLA.Controllers
{
    public class PublicTeacherController : Controller
    {
        private AplaEntities db = new AplaEntities();

        //
        // GET: /PublicTeacher/

        //public ActionResult Index()
        //{
        //    var teachers = db.Teachers.Include(t => t.Race).Include(t => t.Religion);
        //    return View(teachers.ToList());
        //}

        //
        // GET: /PublicTeacher/Details/5
        [Authorize(Roles = "Internal")]
        public ActionResult Details(int id = 0)
        {
            try
            {
                if (id == 0)
                {
                    id = WebSecurity.GetUserId(User.Identity.Name);
                }

                Teacher teacher = db.Teachers.Where(t => t.UserId == id).SingleOrDefault();

                if (teacher == null)
                {
                    return RedirectToAction("Create");
                }

                var currentDistrictResult = db.Districts.Where(d => d.Id == teacher.CurrentDistrictId).FirstOrDefault();
                string currentDistrict = string.Empty;
                if (currentDistrictResult != null)
                {
                    currentDistrict = currentDistrictResult.DistrictName;
                }

                var currentCityResult = db.City.Where(d => d.Id == teacher.CurrentCityId).FirstOrDefault();
                string currentCity = string.Empty;
                if (currentCityResult != null)
                {
                    currentCity = currentCityResult.CityName;
                }

                var permanentDistrictResult = db.Districts.Where(d => d.Id == teacher.PermanentDistrictId).FirstOrDefault();
                string permanentDistrict = string.Empty;
                if (permanentDistrictResult != null)
                {
                    permanentDistrict = permanentDistrictResult.DistrictName;
                }

                var permanentCityResult = db.City.Where(d => d.Id == teacher.PermanentCityId).FirstOrDefault();
                string permanentCity = string.Empty;
                if (permanentCityResult != null)
                {
                    permanentCity = permanentCityResult.CityName;
                }

                ViewBag.CurrentAddress = string.Format("{0},{1},{2}", teacher.CurrentLane, currentCity, currentDistrict);
                ViewBag.permanentAddress = string.Format("{0},{1},{2}", teacher.PermanentLane, permanentCity, permanentDistrict);
                ViewBag.Name = string.Format("{0} {1} {2}", teacher.Name, teacher.CommonName, teacher.FullName);

                return View(teacher);
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                WebSecurity.Logout();
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult TeacherDetails(int id = 0,int page=0)
        {
            try
            {
                ViewBag.Page = page;
                Teacher teacher = db.Teachers.Where(t => t.Id == id).SingleOrDefault();

                if (teacher == null)
                {
                    return RedirectToAction("Index", "Home", null);
                }

                var currentDistrictResult = db.Districts.Where(d => d.Id == teacher.CurrentDistrictId).FirstOrDefault();
                string currentDistrict = string.Empty;
                if (currentDistrictResult != null)
                {
                    currentDistrict = currentDistrictResult.DistrictName;
                }

                var currentCityResult = db.City.Where(d => d.Id == teacher.CurrentCityId).FirstOrDefault();
                string currentCity = string.Empty;
                if (currentCityResult != null)
                {
                    currentCity = currentCityResult.CityName;
                }

                var permanentDistrictResult = db.Districts.Where(d => d.Id == teacher.PermanentDistrictId).FirstOrDefault();
                string permanentDistrict = string.Empty;
                if (permanentDistrictResult != null)
                {
                    permanentDistrict = permanentDistrictResult.DistrictName;
                }

                var permanentCityResult = db.City.Where(d => d.Id == teacher.PermanentCityId).FirstOrDefault();
                string permanentCity = string.Empty;
                if (permanentCityResult != null)
                {
                    permanentCity = permanentCityResult.CityName;
                }

                ViewBag.CurrentAddress = string.Format("{0},{1},{2}", teacher.CurrentLane, currentCity, currentDistrict);
                ViewBag.permanentAddress = string.Format("{0},{1},{2}", teacher.PermanentLane, permanentCity, permanentDistrict);
                ViewBag.Name = string.Format("{0} {1} {2}", teacher.Name, teacher.CommonName, teacher.FullName);

                ViewBag.TimeTables = db.TimeTables.Where(t => t.TeacherId == id).ToList();

                return View(teacher);
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // GET: /PublicTeacher/Create
        [Authorize(Roles = "SuperAdmin,Admin")]
        public ActionResult Create()
        {
            try
            {
                string fmt = "00000.##";
                string NewReg = string.Empty;
                string reg = string.Empty;
                int maxRegNo = 0;
                List<int> numbers = new List<int>();
                int newRegNo = 0;

                var teacherList = db.Teachers;
                if (teacherList != null && teacherList.Count() > 0)
                {
                    foreach (Teacher t in teacherList.ToList())
                    {
                        reg = t.RegNo;
                        int regNo = 0;
                        if (!string.IsNullOrEmpty(reg))
                        {
                            var array = reg.Split('_');
                            regNo = Convert.ToInt32(array[2]);
                            numbers.Add(regNo);
                        }
                    }

                    var max = db.Teachers.OrderByDescending(t => t.RegNo).FirstOrDefault();
                    if (max != null)
                    {
                        var array = max.RegNo.Split('_');
                        maxRegNo = Convert.ToInt32(array[2]);
                    }

                    var result = Enumerable.Range(1, maxRegNo).Except(numbers);
                    if (result != null && result.Count() > 0)
                    {
                        newRegNo = result.FirstOrDefault();
                    }
                    else
                    {
                        newRegNo = maxRegNo + 1;
                    }
                }
                else
                {
                    newRegNo = 1;
                }

                NewReg = string.Format("Apla_{0}_{1}", DateTime.Now.Year, newRegNo.ToString(fmt));

                ViewBag.CityId = new SelectList(db.City, "Id", "CityName");
                ViewBag.DistrictId = new SelectList(db.Districts, "Id", "DistrictName");
                ViewBag.RaceId = new SelectList(db.Races, "Id", "Name");
                ViewBag.ReligionId = new SelectList(db.Religions, "Id", "Name");

                ViewBag.EducationStreams = db.EducationStreams.ToList();
                ViewBag.Institutes = db.Institutes.ToList();
                ViewBag.Occupations = db.Occupations.ToList();
                ViewBag.Reg = NewReg;
                ViewBag.Names = new SelectList((IEnumerable)SelectListItemHelper.GetPersonDetails(), "Text", "Value");
                return View();
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // POST: /PublicTeacher/Create

        [HttpPost]
        [Authorize(Roles = "SuperAdmin,Admin")]
        public ActionResult Create(Teacher teacher, HttpPostedFileBase file)
        {
            try
            {
                bool validationResult = false;
                var keys = Request.Form.AllKeys.ToList();

                foreach (var key in keys)
                {
                    if (key.ToString().StartsWith("s"))
                    {
                        validationResult = true;
                    }
                }

                int iFileSize = 0;

                if (file != null)
                {
                    iFileSize = file.ContentLength;
                }

                if (iFileSize > 5000000)  // 1MB approx (actually less though)
                {
                    // File is too big so do something here
                    ViewBag.Error = "File exceeds the maximum file size 5MB";
                }

                string userName = string.Empty;

                try
                {

                    if (ModelState.IsValid && validationResult && iFileSize < 5000000)
                    {
                        userName = Request["username"];
                        var password = Request["password"];
                        WebSecurity.CreateUserAndAccount(userName, password);
                        var roles = (SimpleRoleProvider)Roles.Provider;
                        if (!roles.RoleExists(Common.UserRoles.Internal.ToString()))
                        {
                            roles.CreateRole(Common.UserRoles.Internal.ToString());
                        }
                        Roles.AddUserToRole(userName, UserRoles.Internal.ToString());

                        int userId = WebSecurity.GetUserId(userName);
                        teacher.UserId = userId;

                        if (file != null)
                        {
                            string pic = System.IO.Path.GetExtension(file.FileName);
                            string path = Server.MapPath("~/Images/ProfileImages/" + teacher.RegNo + pic);
                            file.SaveAs(path);
                        }

                        //adding education streams
                        ICollection<EducationStream> educationStreamList = new HashSet<EducationStream>();
                        var educationStreams = db.EducationStreams.ToList();
                        foreach (EducationStream stream in educationStreams)
                        {
                            var result = Request[Convert.ToString("c" + stream.Id)];
                            if (result != null)
                            {
                                if (result.ToString() == "on")
                                {
                                    educationStreamList.Add(stream);
                                }
                            }
                        }

                        teacher.EducationStreams = educationStreamList;

                        //adding subjects
                        ICollection<Subject> subjectList = new HashSet<Subject>();
                        var subjects = db.Subjects.ToList();
                        foreach (Subject subject in subjects)
                        {
                            var result = Request[Convert.ToString("s" + subject.Id)];
                            if (result != null)
                            {
                                if (result.ToString() == "on")
                                {
                                    subjectList.Add(subject);
                                }
                            }
                        }

                        teacher.Subjects = subjectList;

                        var instituteResult = Request["Ins"];
                        var cityResult = Request["Ins_city"];
                        ICollection<Institute> instituteList = new HashSet<Institute>();

                        if (!string.IsNullOrEmpty(instituteResult))
                        {
                            var instituteListResult = instituteResult.Split(',').ToList();
                            var cityListResult = cityResult.Split(',').ToList();

                            for (int z = 0; z < instituteListResult.Count; z++)
                            {
                                bool createNew = true;
                                string insName = instituteListResult[z];
                                var foundInstitute = db.Institutes.Where(i => i.Name == insName).FirstOrDefault();
                                if (foundInstitute != null)
                                {
                                    if (foundInstitute.CityId == Convert.ToInt32(cityListResult[z]))
                                    {
                                        createNew = false;
                                    }
                                }

                                if (createNew)
                                {
                                    Institute institute = new Institute
                                    {
                                        Name = instituteListResult[z],
                                        CityId = Convert.ToInt32(cityListResult[z])
                                    };
                                    db.Institutes.Add(institute);
                                    db.SaveChanges();
                                    instituteList.Add(institute);
                                }
                                else
                                {
                                    instituteList.Add(foundInstitute);
                                }
                            }
                        }

                        teacher.Institutes = instituteList;


                        //adding Occupations
                        ICollection<Occupation> OccupationList = new HashSet<Occupation>();
                        var occupations = db.Occupations.ToList();
                        foreach (Occupation occupation in occupations)
                        {
                            var result = Request[Convert.ToString("o" + occupation.Id)];
                            if (result != null)
                            {
                                if (result.ToString() == "on")
                                {
                                    OccupationList.Add(occupation);
                                }
                            }
                        }

                        teacher.Occupations = OccupationList;
                        teacher.Enabled = false;
                        teacher.Approved = true;
                        db.Teachers.Add(teacher);
                        db.SaveChanges();

                        ICollection<Children> ChildrenList = new HashSet<Children>();

                        var names = Request["names"];
                        if (!string.IsNullOrEmpty(names))
                        {
                            var bdays = Request["bdays"];
                            var nameList = names.Split(',').ToList();
                            var bdayList = bdays.Split(',').ToList();

                            for (int x = 0; x < nameList.Count(); x++)
                            {
                                Children child = new Children();
                                child.Name = Convert.ToString(nameList[x]);
                                if (bdayList[x] != null)
                                {
                                    try
                                    {
                                        child.Bday = Convert.ToDateTime(bdayList[x]);
                                    }
                                    catch (Exception)
                                    {
                                        child.Bday = DateTime.Now;
                                    }
                                }
                                else { child.Bday = DateTime.Now; }
                                child.TeacherId = teacher.Id;
                                db.Children.Add(child);
                                db.SaveChanges();
                            }

                        }

                        return RedirectToAction("Details");

                    }
                }

                catch (Exception)
                {
                    int userId = WebSecurity.GetUserId(userName);
                    User user = db.Users.Find(userId);
                    db.Users.Remove(user);
                    db.SaveChanges();
                }

                string fmt = "00000.##";
                string NewReg = string.Empty;
                string reg = string.Empty;
                int maxRegNo = 0;
                List<int> numbers = new List<int>();
                int newRegNo = 0;

                var teacherList = db.Teachers;
                if (teacherList != null && teacherList.Count() > 0)
                {
                    foreach (Teacher t in teacherList.ToList())
                    {
                        reg = t.RegNo;
                        int regNo = 0;
                        if (!string.IsNullOrEmpty(reg))
                        {
                            var array = reg.Split('_');
                            regNo = Convert.ToInt32(array[2]);
                            numbers.Add(regNo);
                        }
                    }

                    var max = db.Teachers.OrderByDescending(t => t.RegNo).FirstOrDefault();
                    if (max != null)
                    {
                        var array = max.RegNo.Split('_');
                        maxRegNo = Convert.ToInt32(array[2]);
                    }

                    var result = Enumerable.Range(1, maxRegNo).Except(numbers);
                    if (result != null && result.Count() > 0)
                    {
                        newRegNo = result.FirstOrDefault();
                    }
                    else
                    {
                        newRegNo = maxRegNo + 1;
                    }
                }
                else
                {
                    newRegNo = 1;
                }

                NewReg = string.Format("Apla_{0}_{1}", DateTime.Now.Year, newRegNo.ToString(fmt));


                ViewBag.CityId = new SelectList(db.City, "Id", "CityName");
                ViewBag.DistrictId = new SelectList(db.Districts, "Id", "DistrictName");
                ViewBag.RaceId = new SelectList(db.Races, "Id", "Name");
                ViewBag.ReligionId = new SelectList(db.Religions, "Id", "Name");

                ViewBag.EducationStreams = db.EducationStreams.ToList();
                ViewBag.Institutes = db.Institutes.ToList();
                ViewBag.Occupations = db.Occupations.ToList();
                ViewBag.Reg = NewReg;
                ViewBag.Names = new SelectList((IEnumerable)SelectListItemHelper.GetPersonDetails(), "Text", "Value");
                if (!validationResult)
                {
                    ViewBag.Error = "Please select atleast one subject";
                }
                return View(teacher);
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // GET: /PublicTeacher/Edit/5
        [Authorize(Roles = "Internal")]
        public ActionResult Edit(int id = 0)
        {
            try
            {
                Teacher teacher = db.Teachers.Find(id);
                if (teacher == null)
                {
                    return HttpNotFound();
                }
                ViewBag.CurrentCity = new SelectList(db.City, "Id", "CityName", teacher.CurrentCityId);
                ViewBag.CurrentDistrict = new SelectList(db.Districts, "Id", "DistrictName", teacher.CurrentDistrictId);
                ViewBag.PermanentCity = new SelectList(db.City, "Id", "CityName", teacher.PermanentCityId);
                ViewBag.PermanentDistrict = new SelectList(db.Districts, "Id", "DistrictName", teacher.PermanentDistrictId);
                ViewBag.Race = new SelectList(db.Races, "Id", "Name", teacher.RaceId);
                ViewBag.Religion = new SelectList(db.Religions, "Id", "Name", teacher.ReligionId);
                ViewBag.EducationStreams = db.EducationStreams.ToList();
                ViewBag.Institutes = db.Institutes.ToList();
                ViewBag.Occupations = db.Occupations.ToList();
                ViewBag.Subjects = db.Subjects.ToList();
                ViewBag.Names = new SelectList((IEnumerable)SelectListItemHelper.GetPersonDetails(), "Text", "Value");
                return View(teacher);
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                return RedirectToAction("Details");
            }
        }

        //
        // POST: /PublicTeacher/Edit/5

        [HttpPost]
        [Authorize(Roles="Internal")]
        public ActionResult Edit(Teacher teacher, string[] selectedEducationStreams, string[] selectedOccupations, string[] selectedSubjects,HttpPostedFileBase file)
        {
            try
            {
                int iFileSize = 0;

                if (file != null)
                {
                    iFileSize = file.ContentLength;
                }

                if (iFileSize > 5000000 && iFileSize < 5000000)  // 1MB approx (actually less though)
                {
                    // File is too big so do something here
                    ViewBag.Error = "File exceeds the maximum file size 5MB";
                }

                if (ModelState.IsValid)
                {
                    db.Entry(teacher).State = EntityState.Modified;
                    db.SaveChanges();


                    if (file != null)
                    {
                        try
                        {
                            var imgPath = Directory.EnumerateFiles(Server.MapPath(@"~/Images/ProfileImages/"), "*.*", SearchOption.AllDirectories).Where(s => s.Contains(teacher.RegNo));
                            if (imgPath != null && imgPath.Count() > 0)
                            {
                                foreach (var filePath in imgPath)
                                {
                                    System.IO.File.Delete(filePath);
                                }
                            }
                        }
                        catch (Exception)
                        {

                        }

                        string pic = System.IO.Path.GetExtension(file.FileName);
                        string path = Server.MapPath("~/Images/ProfileImages/" + teacher.RegNo + pic);
                        file.SaveAs(path);
                    }

                    if (selectedEducationStreams != null)
                    {
                        ICollection<EducationStream> educationStreamList = new HashSet<EducationStream>();
                        foreach (var educationStream in db.EducationStreams.ToList())
                        {
                            if (selectedEducationStreams.ToList().Contains(educationStream.Id.ToString()))
                            {
                                educationStream.Teachers.Add(teacher);
                                educationStreamList.Add(educationStream);
                            }
                            else
                            {
                                educationStream.Teachers.Remove(teacher);
                            }
                        }

                        teacher.EducationStreams = educationStreamList;
                    }

                    if (selectedSubjects != null)
                    {
                        //adding subjects
                        ICollection<Subject> subjectList = new HashSet<Subject>();
                        foreach (var subject in db.Subjects.ToList())
                        {
                            if (selectedSubjects.ToList().Contains(subject.Id.ToString()))
                            {
                                subject.Teachers.Add(teacher);
                                subjectList.Add(subject);
                            }
                            else
                            {
                                subject.Teachers.Remove(teacher);
                            }
                        }

                        teacher.Subjects = subjectList;

                    }

                    var instituteResult = Request["Ins"];
                    var cityResult = Request["Ins_city"];
                    ICollection<Institute> instituteList = new HashSet<Institute>();

                    if (instituteResult != null)
                    {
                        var instituteListResult = instituteResult.Split(',').ToList();
                        var cityListResult = cityResult.Split(',').ToList();

                        foreach (var institute in db.Institutes.ToList())
                        {
                            if (institute.Teachers.Contains(teacher))
                            {
                                institute.Teachers.Remove(teacher);
                            }
                        }

                        for (int z = 0; z < instituteListResult.Count; z++)
                        {
                            bool createNew = true;
                            string insName = instituteListResult[z];
                            var foundInstitute = db.Institutes.Where(i => i.Name == insName).FirstOrDefault();
                            if (foundInstitute != null)
                            {
                                if (foundInstitute.City.CityName == cityListResult[z].ToString())
                                {
                                    createNew = false;
                                }
                            }

                            if (createNew)
                            {
                                var newCity = cityListResult[z].ToString();
                                int cityId = 1;
                                var city = db.City.Where(c => c.CityName == newCity).FirstOrDefault();
                                if (city != null)
                                {
                                    cityId = city.Id;
                                }
                                Institute institute = new Institute
                                {
                                    Name = instituteListResult[z],
                                    CityId = cityId
                                };
                                db.Institutes.Add(institute);
                                db.SaveChanges();
                                instituteList.Add(institute);
                            }
                            else
                            {
                                instituteList.Add(foundInstitute);
                            }
                        }
                    }

                    teacher.Institutes = instituteList;

                    if (selectedOccupations != null)
                    {

                        //adding Occupations
                        ICollection<Occupation> OccupationList = new HashSet<Occupation>();
                        foreach (var occupation in db.Occupations.ToList())
                        {
                            if (selectedOccupations.ToList().Contains(occupation.Id.ToString()))
                            {
                                occupation.Teachers.Add(teacher);
                                OccupationList.Add(occupation);
                            }
                            else
                            {
                                occupation.Teachers.Remove(teacher);
                            }
                        }

                        teacher.Occupations = OccupationList;
                    }

                    var names = Request["names"];
                    var bdays = Request["bdays"];

                    string[] nameArray = null;
                    string[] bdayArray = null;

                    if (!string.IsNullOrEmpty(names) && bdays != null)
                    {
                        nameArray = names.Split(',');
                        bdayArray = bdays.Split(',');

                        if (nameArray.Count() > 0)
                        {
                            var childList = db.Children.Where(c => c.TeacherId == teacher.Id).ToList();

                            foreach (var item in childList)
                            {
                                db.Children.Remove(item);
                                db.SaveChanges();
                            }

                            for (int x = 0; x < nameArray.Count(); x++)
                            {
                                Children child = new Children();
                                child.Name = Convert.ToString(nameArray[x]);
                                child.Bday = Convert.ToDateTime(bdayArray[x]);
                                child.TeacherId = teacher.Id;
                                db.Children.Add(child);
                                db.SaveChanges();
                            }
                        }
                    }

                    db.Entry(teacher).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Details");
                }

                ViewBag.Race = new SelectList(db.Races, "Id", "Name", teacher.RaceId);
                ViewBag.Religion = new SelectList(db.Religions, "Id", "Name", teacher.ReligionId);
                ViewBag.CurrentCity = new SelectList(db.City, "Id", "CityName", teacher.CurrentCityId);
                ViewBag.CurrentDistrict = new SelectList(db.Districts, "Id", "DistrictName", teacher.CurrentDistrictId);
                ViewBag.PermanentCity = new SelectList(db.City, "Id", "CityName", teacher.PermanentCityId);
                ViewBag.PermanentDistrict = new SelectList(db.Districts, "Id", "DistrictName", teacher.PermanentDistrictId);
                ViewBag.EducationStreams = db.EducationStreams.ToList();
                ViewBag.Institutes = db.Institutes.ToList();
                ViewBag.Occupations = db.Occupations.ToList();
                ViewBag.Subjects = db.Subjects.ToList();
                ViewBag.Names = new SelectList((IEnumerable)SelectListItemHelper.GetPersonDetails(), "Text", "Value");
                return View(teacher);
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                return RedirectToAction("Details");
            }
        }

        public JsonResult AutoCompleteInstitute(string term)
        {
            var result = (from r in db.Institutes
                          where r.Name.ToLower().Contains(term.ToLower())
                          select new { r.Name }).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AutoCompleteCity(string term)
        {
            var result = (from r in db.City
                          where r.CityName.ToLower().Contains(term.ToLower())
                          select new { r.CityName }).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCityForInstitute(string term)
        {
            var result = (from r in db.Institutes
                          where r.Name.ToLower().Contains(term.ToLower())
                          select new { r.City.CityName }).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult GetCityByDisId(int districtid)
        {
            List<City> objcity = db.City.Where(c => c.DistrictId == districtid).OrderBy(a=>a.CityName).ToList();
            SelectList obgcity = new SelectList(objcity, "Id", "CityName", 0);
            return Json(obgcity, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSubjectByStream(int streamId)
        {
            List<Subject> objcity = db.Subjects.Where(c => c.EducationStreamId == streamId).OrderBy(a=>a.Name).ToList();
            SelectList obgcity = new SelectList(objcity, "Id", "Name", 0);
            return Json(obgcity, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckValidUserName(string userName)
        {
            var result = db.Users.Where(u => u.UserName == userName).SingleOrDefault();
            if (result != null)
            {
                return Json(new { valid = false }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { valid = true }, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult SeachTeacher()
        {
            try
            {
                var educationStream = Request["EducationStreamId"];
                var subject = Request["SubjectId"];
                var district = Request["DistrictId"];
                var city = Request["CityId"];
                var classType = Request["ClassTypeId"];
                var regNo = Request["RegNo"];
                var name = Request["Name"];

                if (!string.IsNullOrEmpty(educationStream) || !string.IsNullOrEmpty(subject) || !string.IsNullOrEmpty(district) || !string.IsNullOrEmpty(city) ||
                    !string.IsNullOrEmpty(classType) || !string.IsNullOrEmpty(regNo) || !string.IsNullOrEmpty(name))
                {
                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    dic.Add("educationStream", educationStream);
                    dic.Add("subject", subject);
                    dic.Add("district", district);
                    dic.Add("city", city);
                    dic.Add("classType", classType);
                    dic.Add("regNo", regNo);
                    dic.Add("name", name);

                    Session.Add("values", dic);
                }
                else
                {
                    string page = string.Empty;

                    var queryString = Request.QueryString.GetValues("grid-page");
                    if (queryString != null && queryString.Count() > 0)
                    {
                        page = queryString.FirstOrDefault();
                    }

                    if (!string.IsNullOrEmpty(page))
                    {
                        ViewBag.Page = page;
                        var dic = Session["values"];
                        if (dic != null)
                        {
                            Dictionary<string, string> values = (Dictionary<string, string>)dic;
                            educationStream = values["educationStream"];
                            subject = values["subject"];
                            district = values["district"];
                            city = values["city"];
                            classType = values["classType"];
                            regNo = values["regNo"];
                            name = values["name"];
                        }
                    }
                    else
                    {
                        Session.Remove("values");
                    }
                }


                var teacherList1 = from t in db.Teachers
                                   where t.Enabled == true
                                   select t;

                if (!String.IsNullOrEmpty(name))
                {
                    var names = name.Split(' ');
                    var firstName = names[0].ToLower();
                    var lastName = string.Empty;

                    if (names.Count() > 1 && !string.IsNullOrEmpty(names[1]))
                    {
                        lastName = names[1].ToLower();
                        teacherList1 = teacherList1.Where(t => t.CommonName.ToLower().Contains(firstName)
                           && t.FullName.ToLower().Contains(lastName));
                    }
                    else
                    {
                        teacherList1 = teacherList1.Where(t => t.CommonName.ToLower().Contains(firstName));
                    }

                }

                var teachers = db.Teachers.Where(t => t.Enabled == true).ToList();

                var teacherList = teacherList1.ToList();

                foreach (Teacher teacher in teachers)
                {
                    if (!string.IsNullOrEmpty(educationStream))
                    {
                        int id = Convert.ToInt32(educationStream);
                        if (id != 0)
                        {
                            if (teacher.EducationStreams.Where(e => e.Id == id).Count() == 0)
                            {
                                teacherList.Remove(teacher);
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(subject))
                    {
                        int id = Convert.ToInt32(subject);
                        if (id != 0)
                        {
                            if (teacher.Subjects.Where(e => e.Id == id).Count() == 0)
                            {
                                teacherList.Remove(teacher);
                            }
                        }
                    }


                    if (!string.IsNullOrEmpty(district))
                    {
                        int id = Convert.ToInt32(district);
                        if (id != 0)
                        {
                            if (teacher.CurrentDistrictId != id)
                            {
                                teacherList.Remove(teacher);
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(city))
                    {
                        int id = Convert.ToInt32(city);
                        if (id != 0)
                        {
                            if (teacher.CurrentCityId != id)
                            {
                                teacherList.Remove(teacher);
                            }
                        }
                    }

                    bool found = false;
                    if (!string.IsNullOrEmpty(classType))
                    {
                        int id = Convert.ToInt32(classType);
                        if (id != 0)
                        {
                            var timeTables = db.TimeTables.Where(t => t.TeacherId == teacher.Id).ToList();
                            foreach (var item in timeTables)
                            {
                                if (item.ClassTypeId == id)
                                {
                                    found = true;
                                }
                            }
                        }

                        if (!found)
                        {
                            teacherList.Remove(teacher);
                        }
                    }


                    if (!string.IsNullOrEmpty(regNo))
                    {
                        if (!teacher.RegNo.Contains(regNo))
                        {
                            teacherList.Remove(teacher);
                        }
                    }
                }

                if (teacherList.Count > 0)
                {
                    return View(teacherList);
                }
                else
                {
                    return RedirectToAction("Index", "Home", null);
                }
            }

            catch (Exception)
            {
                return RedirectToAction("Index", "Home", null);
            }
        }

        public ActionResult Events()
        {
            return View();
        }

        public void SendEmailAdmin(string subject)
        {
            try
            {
                string smtpClient = System.Configuration.ConfigurationManager.AppSettings["SmtpClient"];
                string WebsiteEmail = System.Configuration.ConfigurationManager.AppSettings["WebsiteEmail"];
                string Port = System.Configuration.ConfigurationManager.AppSettings["Port"];
                string Password = System.Configuration.ConfigurationManager.AppSettings["Password"];
                string userEmail = System.Configuration.ConfigurationManager.AppSettings["UserEmail"];

                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                SmtpClient SmtpServer = new SmtpClient(smtpClient);

                mail.From = new MailAddress(WebsiteEmail);
                mail.To.Add(userEmail);
                mail.Subject = subject;
                mail.Body = subject;
                mail.BodyEncoding = Encoding.ASCII;
                mail.IsBodyHtml = true;
                SmtpServer.Port = Convert.ToInt32(Port);
                SmtpServer.Credentials = new System.Net.NetworkCredential(WebsiteEmail, Password);
                //SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError(e.Message);
            }


        }


    }
}